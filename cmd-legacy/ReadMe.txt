STANDARD COMMANDS

Utils
    HELP - prints a short description of given command(s).
    !help hours

    LIST - lists available commands.
    !list
    -You can use a pattern to filter the output:
    !list h*
    -This will print the commands that start with 'h'.
     The '*' symbol stands for any text.

    HOOK - sends given message after a given function is called
    !hook -PlayerBleedOut:_enter I'm down
     send 'I'm down' every time PlayerBleedOut:_enter is called
    !hook -EnemyManager:on_enemy_died !count enemies
     send '!count enemies' when EnemyManager:on_enemy_died is called
    -Use 'unhook' to stop it
     !unhook -PlayerBleedOut:_enter
      stop any messages sent on PlayerBleedOut:_enter
     !unhook I'm down
      stop any messages containing 'I'm down'
     !unhook -PlayerBleedOut:_enter I'm down
      stop any containing 'I'm down' sent on PlayerBleedOut:_enter

    EVENT - sends given message after a given event happens
     this command is an easier interface for 'hook' command
    !event -EnemyDied !count enemies
     send '!count enemies' every time an enemy dies
    !event -CameraInteract Camera looped
     send 'Camera looped' every time you loop a camera
    -Use 'event-stop' to stop messages
     !event-stop -Tased
      stop any messages sent when you get tased
     !event-stop Camera
      stop any messages that contain 'Camera'
     !event-stop -CameraInteract Camera looped
      stop messages containing 'Camera looped' and sent
       after you interact with a camera
    -Use '-list' option to see available events
     !event -list

Game
    FPS - caps frames per second rate at a given value
    !fps 60
    
    COUNT - counts objects
    !count enemies
    !count persons
    -It can count the total amount of medic bag charges, 
     amount of ammo in all ammobags or body bag cases:
    !count meds
    -List of common arguments:
     pickups enemies persons all civilians 
     all_criminals meds ammo bodybags bags
    
    BIG OIL CALCULATOR - tells you which engine matches your intel
    !boc he 3 <
    !boc d2h
    -The second one is short form ('l' for 'lower' pressure,
	 'h' for 'higher').
    -For long form arguments are he, ni, de, 1, 2, 3, <, >.
     
     PRIVATE MESSAGE - Sends private message to given player(s)
     !pm -andole
      add players with 'andole' in the name to the list of
      recipients
     !pm hello
      send 'hello' to every player in the recipients list
     !pm-clear
      clears the recipients list
    
Tools
    HOURS - shows playtime of given player(s)
    !hours -all
    !hours andole
     You can use incomplete player's name. It will find him.
    !hours -2
     Shows playtime of player #2 (1-host, 2-blue , 3-red, 4-orange).
    
    SKILLS - shows skills and perk deck of given player(s)
    !skills -all
    !skills andole
    !skills -2
    
    DICTIONARY - translates words using Multitran.com
    options:
        -(l1)-(l2) 
         changes languages for 1 translation
        -set 
         used after the previous one, switches
          the command to those languages
        -lang 
         prints current languages
        -(num) 
         sets the number of translations for each word
    examples:
        dict -ru-en -set 
         sets current language pair to ru-en
        dict -en-ru house 
         transtales 'house' into russian, lang pair stays unchanged
        dict house
         this will translate 'house' from and into
          languages of the current lang pair
        dict -r 
         reverses the lang pair and sets it as current
    -Usually, you will only need to set the lang pair and then
     simply use '!dict word'
     
    WIKIPEDIA - searches Wikipedia for given word(s)
    options:
        -(lang)
         changes language of the wiki
        -(num)
         acesses the necessary page,
          when a choice is given
    examples:
        wiki -es
         switches to spanish wiki
        wiki flash
         you'll get several options
        wiki -3 flash
         picks option #3 of 'flash' request
    
    CALCULATOR - a simple calculator
    example:
        calc 4+(9-3*9)/(2^(25%3))

Other
    ONE-LINERS - prints one-liners into chat
    !1liners
     prints a random one-liner
    !1liners 32
     prints one-liner #32
    !1liners All*
     prints one-liners that start with 'All'
     the '*' symbol stands for any text

     BASH - prints quotes from bash.org
     !bash
      prints a random quote
     !bash 32
      prints quote #32
