local thisPath, thisDir
local function GetPath()
	thisPath = debug.getinfo(2, "S").source:sub(2)
	thisDir = string.match(thisPath, '.*/')
end
GetPath()
GetPath = nil

dofile(thisDir..'pc/pc/PowerChat.lua')

if not PowerChat then
	log('Failed to load PowerChat')
	return
end


dofile(thisDir..'cmd/load_all.lua')

PowerChat:AddCommandsFile(thisDir..'cmd/help.pc')
PowerChat:AddCommandsFile(thisDir..'cmd/commands.pc')
PowerChat:AddCommandsFile(SavePath..'autoexec.pc')

PowerChat2 = PowerChat2 or {}
PowerChat2.cmd_path = thisDir..'pc/cmd/'
PowerChat2.path = thisDir


PowerChat:AddExternal('pc2', PowerChat2.commands, function(name, cmd, args, ops, raw, out, self)
	return cmd(self, args, ops, raw, out)
end)


if PowerChat2.menu and not PowerChat2.menu.load_up then
	log('PowerChat is disabled in the menu')
	return
end


local control = PowerChat2.menu and PowerChat2.menu.control_char[PowerChat2.menu.control_char.value] or '!'
local PC


local to_everybody = false
local no_parse = false
local Out = function(str, chatName, col)
	if type(str) ~= 'string' then return end

	if PC then
		table.insert(PC.last_output[2], str)
	end

	chatName = type(chatName) == 'string' and '['..chatName..']' or '[PC]'

	if managers and managers.chat then
		if to_everybody then
			local peer = managers.network and managers.network:session() or nil
			peer = peer and peer:local_peer() or nil
			no_parse = true
			managers.chat:send_message(1, peer, str)
		else
			managers.chat:_receive_message(1, chatName, str, col or Color.green)
		end
	else
		log(str)
	end
end


PC = PowerChat2:NewInstance('PowerChat', Out, control, PowerChat2.cmd_path)
if not PC then return end


local CM_send_orig = ChatManager.send_message
function ChatManager:send_message(channel_id, sender, message, ...)
	if channel_id ~= 1 then
		return CM_send_orig(self, channel_id, sender, message, ...)
	end

	PC.last_output[1] = PC.last_output[2]
	PC.last_output[2] = {}

	if no_parse then
		no_parse = false
		CM_send_orig(self, channel_id, sender, message, ...)
	else
		if message:match('^'..control..control) then
			to_everybody = true
			message = message:sub(2)
		end

		local s, needs_out = PC:Process(message)

		if s ~= '' then
			if needs_out then
				Out(s)
			else
				CM_send_orig(self, channel_id, sender, s, ...)
			end
		elseif needs_out then--or to_everybody then
			CM_send_orig(self, channel_id, sender, message, ...)
		end

		to_everybody = false
	end
end



-- Legacy support

function PowerChat:OutPut(chatName, str, col)
	if str:len() > 325 then
		str = str:sub(1, 322) .. '...'
	end
	Out(str, chatName, col)
end

PowerChatCommands = PowerChatCommands or {}

PowerChat:AddExternal('pc1',PowerChatCommands, function(name, cmd, args, ops, raw)
	local func = cmd.Parse_Return or cmd.Parse
	table.insert(args, 1, '')
	for k,v in ipairs(ops) do
		ops[k] = '-'..v
	end
	return func(args, ops)
end)


PowerChat.settings = {
	sym = '!',
	opt_sym = '-',
	repl_sym = '$',
	and_sym = '&',
	print_cmd = 'false',
	hijack_chat = 'true'
}


dofile(thisDir..'cmd-legacy/game/Count.lua')
dofile(thisDir..'cmd-legacy/game/BigOilCalc.lua')
dofile(thisDir..'cmd-legacy/utils/Help.lua')
dofile(thisDir..'cmd-legacy/utils/List.lua')
dofile(thisDir..'cmd-legacy/utils/Hook.lua')
dofile(thisDir..'cmd-legacy/utils/Event.lua')
dofile(thisDir..'cmd-legacy/other/One-Liners.lua')
dofile(thisDir..'cmd-legacy/tools/Dictionary.lua')
dofile(thisDir..'cmd-legacy/tools/Wikipedia.lua')
dofile(thisDir..'cmd-legacy/game/Fps.lua')
dofile(thisDir..'cmd-legacy/other/Bash.lua')
dofile(thisDir..'cmd-legacy/tools/Hours.lua')
dofile(thisDir..'cmd-legacy/tools/Skills.lua')
dofile(thisDir..'cmd-legacy/game/PrivateMessage.lua')



-- Connect other command mods if present

if ChatCommand and ChatCommand.CMD_ACCESS and not ChatCommand.no_powerchat then
	dofile(thisDir..'hijack/rtd.lua')
end

if Console and Console.command_list and not Console.no_powerchat then
	dofile(thisDir..'hijack/dc.lua')
end
