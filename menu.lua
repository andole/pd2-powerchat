if not MenuHelperHelper then return end

PowerChat2 = PowerChat2 or {}

PowerChat2.menu = {
	control_char = { '!', '\\', '@', '`', '$', '^', '?', '|', value = 1 },
	load_up = true
}

MenuHelperHelper:CreateMenu(PowerChat2.menu, 'powerchat', 'PowerChat.txt', function(settings, menu_id) 
    PowerChat2.menu = settings
end)


local thisPath, thisDir
local function GetPath()
	thisPath = debug.getinfo(2, "S").source:sub(2)
	thisDir = string.match(thisPath, '.*/')
end
GetPath()
GetPath = nil

Hooks:Add("LocalizationManagerPostInit", "PowerChat", function(loc)
    LocalizationManager:load_localization_file(thisDir..'loc.json')
end)