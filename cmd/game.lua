PowerChat2.commands = PowerChat2.commands or {}

PowerChat2.commands.res = function(self, args, ops, raw)
	if Network:is_client() or not CopDamage then return end
	managers.vote:restart_auto()
end


PowerChat2.commands.clear = function(self)
	if managers.menu_component._game_chat_gui then
		local l = managers.menu_component._game_chat_gui._lines or {}
		local pnl = managers.menu_component._game_chat_gui._panel
		pnl = pnl and pnl:child("output_panel") or nil
		pnl = pnl and pnl:child("scroll_panel") or nil
		if pnl then
			for k,v in pairs(l) do
				for k2,v2 in pairs(v) do
					pnl:remove(v2)
				end
			end
			managers.menu_component._game_chat_gui._lines = {}
		end
	end
end
