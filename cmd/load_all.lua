local thisPath, thisDir
local function GetPath()
	thisPath = debug.getinfo(2, "S").source:sub(2)
	thisDir = string.match(thisPath, '.*/')
end
GetPath()
GetPath = nil


local pc_dir = 'PowerChat/'

if not file.DirectoryExists(pc_dir) then
	file.CreateDirectory(pc_dir)
end

PowerChat2 = PowerChat2 or {}
function PowerChat2:NewInstance(...)
	local ni = PowerChat:NewInstance(...)

	if ni then
		-- for !again
		ni.last_output = { {}, {} }

		local dirs = file.GetDirectories(pc_dir)
		for k,v in pairs(dirs or {}) do
			ni.load_dir = pc_dir..v..'/'
			ni.commands.exec({pc_dir..v..'/load.pc'},{})
			ni.load_dir = nil
		end

		ni.commands.namespace({},{})
	end

	return ni
end


dofile(thisDir..'utils.lua')
dofile(thisDir..'game.lua')
