PowerChat2.commands = PowerChat2.commands or {}

PowerChat2.commands.again = function(self, args, ops, raw, out)
	if self.last_output then
		for k,v in ipairs(self.last_output[1]) do
			out(v,'AGAIN')
		end
	end
end


PowerChat2.commands.autoexec = function(self, args, ops, raw)
	local text = args[1]

	if not text then
		if not ops[1] then return end

		local c, ext, ext_name, tbl = self:ResolveNamespace(ops[1])
		local cmd = ext or c

		if not cmd or type(cmd) ~= 'string' then
			return
		end

		text = cmd
	end

	local file = io.open(SavePath..'autoexec.pc', 'a')
	if file then
		local regex = '^'..self.parser.syms.block_start..'(.*)'..self.parser.syms.block_fin..'$'
		text = text:gsub(regex, '%1',1)
		file:write(text..'\n')
	end

	file:close()
end

PowerChat2.commands['autoexec-clear'] = function(self, args, ops, raw)
	os.remove(SavePath..'autoexec.pc')
end


PowerChat2.commands.exec = function(self, args, ops, raw)
	local old_enabled = self.enabled
	self.enabled = true
	if self.load_dir and self:Exec(self.load_dir..args[1], ops[1]) then
		return self.exec_return or ''
	end
	local ret = self.commands.exec({args[1]},{})
	self.enabled = old_enabled
	return ret
end


PowerChat2.commands['exec-lua'] = function(self, args, ops, raw)
	local function exists(fn)
		local f = io.open(fn, 'r')
		if f then
			f:close()
			return true
		end
		return false
	end

	if self.load_dir and exists(self.load_dir..args[1]) then
		return dofile(self.load_dir..args[1])
	end
	if exists(thisDir..args[1]) then
		return dofile(thisDir..args[1])
	end
	if exists(args[1]) then
		return dofile(args[1], ops[1])
	end
	self:Out('Could not open file: '..args[1])
end


PowerChat2.commands.dc = function(self, args, ops, raw)
	local t = tonumber(ops[1])
	if args[1] and t and DelayedCalls and DelayedCalls.Add then
		DelayedCalls:Add('PowerChat_'..tostring(os.time()), t, function()
			local cmd = self.parser.syms.cmd .. args[1]
            if managers.chat then
                managers.chat:send_message(1, "[PC]", cmd)
            end
		end)
	end
end
