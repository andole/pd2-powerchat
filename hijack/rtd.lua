
-- https://github.com/DrNewbie/ChatCommand

local chat_init_orig = ChatManager.init
function ChatManager:init(...)
	chat_init_orig(self, ...)
	local cc_cmd = {}
	for k,v in pairs(self._commands) do
		cc_cmd[k:sub(2)] = v
	end
	
	local cc_func = function(name, cmd, args, ops, message)
		local newname = name
		message = '!'..(newname or name)..' '..message
		local cmm = managers.chat
		local peer = managers.network:session():local_peer()
		local is_run_by_Host = function ()
			if not Network then
				return false
			end
			return not Network:is_client()
		end
		local commad = string.lower(tostring(message))
		local _is_Host = peer:id() == 1 --HOST
		local _is_VIP = ChatCommand:is_VIP(peer) --VIP
		local _is_rHost = is_run_by_Host() --Is this only run by Host
		local type1, type2, type3 = unpack(commad:split(" "))
		if _is_rHost then
			if type1 and (type1:sub(1,1) == "!" or type1:sub(1,1) == "/") and cmm._commands and cmm._commands[string.lower(type1)] then
				if (cmm._commands[string.lower(type1)].ishost and _is_Host) or (cmm._commands[string.lower(type1)].isvip and _is_VIP) or (not cmm._commands[string.lower(type1)].ishost and not cmm._commands[string.lower(type1)].isvip) then
					if managers.trade and managers.trade.is_peer_in_custody and managers.trade:is_peer_in_custody(peer:id()) then
						managers.chat:say("Sorry, [".. tostring(peer:name()) .."] you're in custody")
					else
						cmm._commands[string.lower(type1)].func(peer, type1, type2, type3)					
					end
				else 
					cmm:say("You don't have premission to use this command")
				end
			elseif type1 and (type1:sub(1,1) == "!" or type1:sub(1,1) == "/") then
				cmm:say("The command: " .. type1 .. " doesn't exist")
			end
		end
	end
	
	PowerChat:AddExternal('cc',cc_cmd, cc_func)
end
