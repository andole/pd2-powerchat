
-- https://github.com/offyerrocker/Console

-- External handler
local function dc_func(name, cmd, pc_args, ops,input_str, output)
	--input_str = '/'..name..' '..input_str

	local result = input_str
	
	local func
	local error_msg
	local args,command
	local argument_string = ""
	local postargs
	
	string.gsub(result,"'","\\'")
	
	command = cmd
	
	postargs = command.postargs
	args = Console:split_two(result,postargs)

	if #args > 0 then 
		argument_string = "'" .. string.gsub(table.concat(args,","),",","','") .. "'"
	end
	if command.str then 
		result = string.gsub(command.str,"$ARGS",argument_string)
	else 
		return
	end
	
	func, error_msg = loadstring('return '..result)
	if func then
		local oldlog = Console.Log
		local newout = function(a,b) 
			b = tostring(b)
			if a.powerchat then
				oldlog(a, b) 
			else 
				output(b) 
			end
		end
		Console.Log = output and newout or Console.Log
		local ret = func()
		Console.Log = oldlog
		return tostring(ret or '')
	end
end

-- Add DC commands to all PowerChats
PowerChat:AddExternal('dc',Console.command_list, dc_func)

PowerChat:AddExternal('dc_pc',{ font = function(args, ops, raw) 
						Console.settings.font_size = tonumber(args[1]) or 12
						end})


local PC_Console

-- Multiline output support
local dc_log_orig = Console.Log
function Console:Log(info, params, ...)
	if params and self.pc_no_log_color then
		params.color = Color.white:with_alpha(1)
		self.pc_no_log_color = false
	end
	info = tostring(info)
	for v in info:gmatch('[^\n]+') do
		if PC_Console then
			table.insert(PC_Console.last_output[2], v)
		end
		dc_log_orig(self, v, params, ...)
	end
end

-- Output function for PowerChat inside DC
local Console_Log = function(str,...)
	if type(str) ~= 'string' then return end
	
	Console.powerchat = true
	Console:Log(str,...)
	Console.powerchat = false
end


local control = PowerChat2.menu and PowerChat2.menu.control_char[PowerChat2.menu.control_char.value] or '!'
-- Create a new PowerChat for DC
PC_Console = PowerChat2:NewInstance('PowerChatConsole', Console_Log, control, PowerChat2.cmd_path)
if not PC_Console then return end

-- Hook to the console
local c_ii_orig = Console.InterpretInput
function Console:InterpretInput(str, ...)
	if str:match(PC_Console.parser.regexes.belongs) then
		Console:Log(str, {new_cmd = true,color = Color.white:with_alpha(0.7),h_margin = 0})
	end
	
	table.remove(PC_Console.last_output[2])
	
	PC_Console.last_output[1] = PC_Console.last_output[2]
	PC_Console.last_output[2] = {}

	local s, needs_out = PC_Console:Process(str)
	
	self.pc_no_log_color = false
	
	if s ~= '' then
		if needs_out then
			self.pc_no_log_color = true
			return function() return s end
		else
			return c_ii_orig(Console, s, ...)
		end
	elseif needs_out then
		return c_ii_orig(Console, str, ...)
	end
	
	self.pc_no_log_color = true
	return function() return s end
end
